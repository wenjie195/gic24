<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
// require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$username = $_SESSION['username'];
$projectNameNew = "";
$projectDetails = getProject($conn);
$totalClaim1st = 0;
$totalClaim2nd = 0;
$totalClaim3rd = 0;
$totalClaim4th = 0;
$totalClaim5th = 0;
$totalClaim = 0;
// $projectName = "";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Status Claim | GIC" />
    <title>Status Claim | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'admin1Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">



    <h1 class="h1-title h1-before-border shipping-h1 status-h1">Status Claims</h1>
    <a href="">
        <div class="five-red-btn-div right-red-btn">
            <p class="short-p five-red-p g-first-3-p n-p">ALL STATUS</p>
        </div>
    </a>
    <div class="short-red-border status-red-border"></div>

	<div class="width100 overflow section-divider">
		<div class="left-status-div1">
        	<div class="orange-status status-div1"></div> <span>In Progress Claim</span>
    	</div>
		<div class="left-status-div1 second-left-status-div">
        	<div class="red-status status-div1"></div> <span>Unclaimed</span>
    	</div>
    </div>
	<div class="clear"></div>
    <div class="section-divider width100 overflow">
      <?php $loanDetails = getLoanStatus($conn, "WHERE agent = ? ORDER BY project_name ASC", array("agent"), array($username), "s");
      if ($loanDetails) {
        for ($cnt=0; $cnt <count($loanDetails) ; $cnt++) {
          $totalClaim1st = 0;
          $totalClaim2nd = 0;
          $totalClaim3rd = 0;
          $totalClaim4th = 0;
          $totalClaim5th = 0;
          $totalClaim = 0;
          $claim1st = $loanDetails[$cnt]->getClaimAmt1st();
          $claim2nd = $loanDetails[$cnt]->getClaimAmt2nd();
          $claim3rd = $loanDetails[$cnt]->getClaimAmt3rd();
          $claim4th = $loanDetails[$cnt]->getClaimAmt4th();
          $claim5th = $loanDetails[$cnt]->getClaimAmt5th();
         if ($claim1st) {
           $totalClaim1st = 1;
         }
         if ($claim2nd) {
           $totalClaim2nd = 1;
         }
         if ($claim3rd) {
           $totalClaim3rd = 1;
         }
         if ($claim4th) {
           $totalClaim4th = 1;
         }
         if ($claim5th) {
           $totalClaim5th = 1;
         }

        $projectName = $loanDetails[$cnt]->getProjectName();

        $projectDetails = getProject($conn, "WHERE project_name = ?",array("project_name"), array($projectName), "s");
        $projectClaims = $projectDetails[0]->getProjectClaims();

        $totalClaim = $totalClaim1st + $totalClaim2nd + $totalClaim3rd + $totalClaim4th + $totalClaim5th;
        $totalUnclaim = $projectClaims - $totalClaim;

            ?><div class="four-column-div">
                  <div class="circle-div">
                      <p class="orange-text status-big"><?php echo $totalClaim ?>/<?php echo $projectClaims ?></p>
                        <p class="red-text status-big"><?php echo $totalUnclaim ?>/<?php echo $projectClaims ?></p>
                        <p class="small-status"><?php echo $loanDetails[$cnt]->getProjectName() ?></p>
                        <p class="small-status"><?php echo $loanDetails[$cnt]->getUnitNo() ?></p>
                    </div>
                    <a><div class="red-btn">STATUS</div></a>
                </div><?php

        }
      } ?>

    </div>


    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->
    <!-- End of Filter -->
    <div class="clear"></div>

</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
</body>
</html>
