<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn," WHERE user_type=3");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Add Project | GIC" />
    <title>Add Project | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  a{
    color: red;
  }
</style>
<body class="body">
<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Add New Project</h1>
    <div class="short-red-border"></div>

    <form method="POST" action="utilities/addProjectFunction.php">
        <label class="labelSize">Project Name : <a>*</a></label>
        <input  class="inputSize input-pattern" type="text"  placeholder="Project Name" name="project_name" id="project_name"><br>

        <label class="labelSize">Person Incharge:</label>
        <input  class="inputSize input-pattern" type="text" placeholder="Person Incharge" name="add_by" id="add_by"><br>

        <label class="labelSize">Project Leader: <a>*</a></label><img style="cursor: pointer" id="remBtn" width="13px" align="right" src="img/mmm.png"><img style="cursor: pointer" id="addBtn" width="13px" align="right" src="img/ppp.png">
        <select class="inputSize input-pattern" name="project_leader[]">
          <option value="">Select a option</option>
          <?php if ($userDetails) {
            for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
              ?><option value="<?php echo $userDetails[$cnt]->getUsername() ?>"><?php echo $userDetails[$cnt]->getUsername() ?></option> <?php
            }
          } ?>
        </select> <br>
        <label style="display: none" class="labelSize1">Project Leader: <a>*</a></label>
        <select id="project_leader1" disabled style="display: none" class="inputSize input-pattern" name="project_leader[]">
          <option value="">Select a option</option>
          <?php if ($userDetails) {
            for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
              ?><option value="<?php echo $userDetails[$cnt]->getUsername() ?>"><?php echo $userDetails[$cnt]->getUsername() ?></option> <?php
            }
          } ?>
        </select>
        <label style="display: none" class="labelSize2">Project Leader: <a>*</a></label>
        <select id="project_leader2" disabled style="display: none" class="inputSize input-pattern" name="project_leader[]">
          <option value="">Select a option</option>
          <?php if ($userDetails) {
            for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
              ?><option value="<?php echo $userDetails[$cnt]->getUsername() ?>"><?php echo $userDetails[$cnt]->getUsername() ?></option> <?php
            }
          } ?>
        </select>
        <label style="display: none" class="labelSize3">Project Leader: <a>*</a></label>
        <select id="project_leader3" disabled style="display: none" class="inputSize input-pattern" name="project_leader[]">
          <option value="">Select a option</option>
          <?php if ($userDetails) {
            for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
              ?><option value="<?php echo $userDetails[$cnt]->getUsername() ?>"><?php echo $userDetails[$cnt]->getUsername() ?></option> <?php
            }
          } ?>
        </select>
        <label style="display: none" class="labelSize4">Project Leader: <a>*</a></label>
        <select id="project_leader4" disabled style="display: none" class="inputSize input-pattern" name="project_leader[]">
          <option value="">Select a option</option>
          <?php if ($userDetails) {
            for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
              ?><option value="<?php echo $userDetails[$cnt]->getUsername() ?>"><?php echo $userDetails[$cnt]->getUsername() ?></option> <?php
            }
          } ?>
        </select>

        <label class="labelSize">Company Name : <a>*</a></label>
        <input  class="inputSize input-pattern" type="text"  placeholder="Company Name" name="company_name" id="project_name"><br>

        <label class="labelSize">Company Address : <a>*</a></label>
        <input  class="inputSize input-pattern" type="text"  placeholder="Company Address" name="company_address" id="project_name"><br>

        <label class="labelSize">No. of Claim Stage: <a>*</a></label>
        <input class="inputSize input-pattern" type="number" placeholder="numbers of claims stages" name="claims_times" id="claims_times"><br>

        <!-- <input type="hidden" name="add_by" id="add_by" value="<?php //echo $userDetails->getUsername(); ?>"> -->

        <button class="button" type="submit" name="loginButton">Add Project</button><br>
    </form>

<?php $projectDetails = getProject($conn); ?>
<?php if ($projectDetails) {

 ?>
    <h2>Project List</h2>

    <table class="shipping-table">
    <tr>
        <th>No.</th>
        <th>Project Name</th>
        <th>Person In Charge (PIC)</th>
        <th>Project Leader</th>
        <th>Company Name</th>
        <th>Company Address</th>
        <th>Project Claim</th>
        <th>Display</th>
        <th>Date Created</th>
        <th>Date Updated</th>
        <th>Action</th>
        <!-- <th>Invoice</th> -->
    </tr>
    <?php for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {

     ?>
    <tr>
    <?php
    ?>  <td class="td"><?php echo $cnt+1 ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getProjectName() ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getAddProjectPpl() ?></td>
        <td class="td"><?php echo str_replace(",","<br>",$projectDetails[$cnt]->getProjectLeader()); ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getCompanyName() ?></td>
        <td class="td"><?php echo str_replace(",","<br>",$projectDetails[$cnt]->getCompanyAddress()) ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getProjectClaims() ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getDisplay() ?></td>
        <td class="td"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateCreated())) ?></td>
        <td class="td"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateUpdated())) ?></td>
        <td class="td">  <form action="editProject.php" method="POST">
              <button class="clean edit-anc-btn hover1" type="submit" name="project_id" value="<?php echo $projectDetails[$cnt]->getId();?>">
                  <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
                  <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
              </button>
          </form></td>
    </tr>
    <?php
     } ?>
    </table>
<?php } ?>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
</body>
<script type="text/javascript">
$(document).ready(function(){
var a = 0;
$("#remBtn").hide();
  $("#addBtn").click( function(){
    a++;
    if (a == 1) {
      $(".labelSize1").fadeIn(500);
      $("#project_leader1").fadeIn(500);
      $("#project_leader1").removeAttr('disabled');
      $("#remBtn").show();
    }
    if (a == 2) {
      $(".labelSize2").fadeIn(500);
      $("#project_leader2").fadeIn(500);
      $("#project_leader2").removeAttr('disabled');
    }
    if (a == 3) {
      $(".labelSize3").fadeIn(500);
      $("#project_leader3").fadeIn(500);
      $("#project_leader3").removeAttr('disabled');
    }
    if (a == 4) {
      $(".labelSize4").fadeIn(500);
      $("#project_leader4").fadeIn(500);
      $("#project_leader4").removeAttr('disabled');
    }
    if (a > 3) {
      $("#addBtn").hide();
      $("#remBtn").show();
    }

  });
  // var b = 0;
  $("#remBtn").click( function(){
    a--;
    if (a < 0) {
      a = 1;
    }
    if (a == 0) {
      $(".labelSize1").fadeOut(500);
      $("#project_leader1").fadeOut(500);
      $("#project_leader1").prop('disabled',true);
      $("#addBtn").show();
      $("#remBtn").hide();
    }
    if (a == 1) {
      $(".labelSize2").fadeOut(500);
      $("#project_leader2").fadeOut(500);
      $("#project_leader2").prop('disabled',true);
    }
    if (a == 2) {
      $(".labelSize3").fadeOut(500);
      $("#project_leader3").fadeOut(500);
      $("#project_leader3").prop('disabled',true);
    }
    if (a == 3) {
      $(".labelSize4").fadeOut(500);
      $("#project_leader4").fadeOut(500);
      $("#project_leader4").prop('disabled',true);
      $("#addBtn").show();
    }

  });
  });
</script>
</html>
