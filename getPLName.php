<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$projectName = $_POST['projectName'];

$projectDetails = getProject($conn, "WHERE project_name = ? ",array("project_name"),array($projectName), "s");

$plName = $projectDetails[0]->getProjectLeader();
$plNameExplode = explode(",",$plName);
for ($cnt=0; $cnt <count($plNameExplode) ; $cnt++) {
  $plNameArray[] = array("totalPLName" => $plNameExplode[$cnt]);
}
;

echo json_encode($plNameArray);
 ?>
