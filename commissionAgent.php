<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Commission.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/ringgitMalaysia.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$finalAmountFinal = 0;
$af = 0;
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
if (isset($_POST['headerCheck']) && isset($_POST['selectedCommSlip'])) {
  $id = $_POST['selectedCommSlip'];
  $idImplode = implode(",", $id);
  $idExplode = explode(",",$idImplode);
  $claimType = 'Multi';
  $claimId = md5(uniqid());
  for ($cnt=0; $cnt <count($idExplode) ; $cnt++) {
  $commissionDetails = getCommission($conn, "WHERE id = ?",array("id"),array($idExplode[$cnt]),"s");
  $uplineName = $commissionDetails[0]->getUpline();
  $uplineNameArray[] = $uplineName;
}
if ($uplineNameArray) {
  $uplineNameLatest = implode(",",$uplineNameArray);
  $uplineNameLatestExplode = explode(",",$uplineNameLatest);
  for ($cnt=0; $cnt <count($uplineNameLatestExplode) ; $cnt++) {
  $cntNotAdd = $cnt;
    $cntADD = $cnt+1;
    if (isset($uplineNameLatestExplode[$cntADD])) {
      if ($uplineNameLatestExplode[$cntNotAdd] != $uplineNameLatestExplode[$cntADD]) {
        $_SESSION['messageType'] = 1;
        header('location: ./adminCommission.php?type=7');
      }
    }

  }


}
}elseif (isset($_POST['headerCheck']) && !isset($_POST['selectedCommSlip'])) {
  $_SESSION['messageType'] = 1;
  header('location: ./adminCommission.php?type=6');
}
else {
  $commissionDetails = getCommission($conn, "WHERE id =?",array("id"),array($_POST['id']), "s");

  $userDetails = getUser($conn, "WHERE username = ? ", array("username"), array($commissionDetails[0]->getUpline()), "s");

  $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?", array("loan_uid"), array($commissionDetails[0]->getLoanUid()), "s");
  $nettPrice = $loanDetails[0]->getNettPrice();
  // $commissionDetails = getInvoice($conn, "WHERE id =? ORDER BY id DESC LIMIT 1",array("id"),array($_POST['invoice']), "s");
  $id = $_POST['id'];
  $claimType = 'Single';
  $claimId = md5(uniqid());
}

// $projectName = $_POST['project_name'];
$status = 'COMPLETED';

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Commission | GIC" />
    <title>Commission | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1"   onclick="goBack()">
    	<a  class="black-white-link2 hover1">
    	    <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	Commission
        </a>
    </h1>
    <div class="spacing-left short-red-border"></div>
    <!-- This is a filter for the table result -->


    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 print-div">
 		<div class="text-center">
            <img src="img/gic-logo.png" class="invoice-logo" alt="GIC Consultancy Sdn. Bhd." title="GIC Consultancy Sdn. Bhd.">
            <p class="invoice-address company-name"><b>GIC Consultancy Sdn. Bhd.</b></p>
            <p class="invoice-small-p">1189044-D (SST No : P11-1808-31038566)</p>
            <p class="invoice-address">1-2-42, Elit Avenue, Jalan Mayang Pasir 3, Bayan Lepas 11950 Penang.</p>
            <p class="invoice-address">Email: gicpenang@gmail.com  Tel:04-6374698</p>
        </div>
		<h1 class="invoice-title">COMMISSION / ALLOWANCE</h1>
       <div class="invoice-width50 top-invoice-w50">
			<table class="invoice-top-small-table left-table">
            	<tr>
                	<td>Date</td>
                    <td>:</td>
                    <td><?php echo date('d/m/Y',strtotime($commissionDetails[0]->getDateCreated())); ?></td>
                </tr>
                <tr>
                	<td>Pay to</td>
                    <td>:</td>
                    <td><?php     if ($commissionDetails[0]->getUpline()) {
                          $userDetails = getUser($conn,"WHERE username = ?",array("username"),array($commissionDetails[0]->getUpline()),"s");
                          if ($userDetails) {
                            echo $userDetails[0]->getFullName();
                          }

                        } ?></td>
                </tr>
            </table>
        </div>
        <div class="invoice-width50 top-invoice-w50">
			<table class="invoice-top-small-table">
            	<tr>
                	<td>Payslip No</td>
                    <td>:</td>
                    <td><?php
                    if (isset($_POST['headerCheck']) && isset($_POST['selectedCommSlip'])) {
                      if ($commissionDetails[0]->getDateCreated()) {
                        echo date('ymd',strtotime($commissionDetails[0]->getDateCreated())).str_replace(",","",$idImplode);
                      }
                    }else {
                      echo date('ymd',strtotime($commissionDetails[0]->getDateCreated())).str_replace(",","",$id);
                    } ?></td>
                </tr>
                <tr>
                	<td>NRIC</td>
                    <td>:</td>
                    <td><?php
            if ($commissionDetails) {
              echo $commissionDetails[0]->getIcNo();
            }
             ?></td>
                </tr>
            </table>
        </div>





        <div class="clear"></div>
        <table class="invoice-printing-table">
        	<thead>
                    <tr>
                    	<!-- <th >No.</th>
                        <th >Items</th>
                        <th >Amount (RM)</th> -->
                        <th>Project</th>
                        <th>Details</th>
                        <th>Unit</th>
                        <th>Nett Price (RM)</th>
                        <th>Amount (RM)</th>
                    </tr>
            </thead>

<?php if (isset($_POST['headerCheck']) && isset($_POST['selectedCommSlip'])) {
  for ($cnt=0; $cnt <count($idExplode) ; $cnt++) {
  $commissionDetails = getCommission($conn, "WHERE id = ?",array("id"),array($idExplode[$cnt]),"s");
  $loanUid = $commissionDetails[0]->getLoanUid();
  $loanDetails = getLoanStatus($conn, "WHERE loan_uid = ?",array("loan_uid"),array($loanUid), "s");
  $finalAmount = $commissionDetails[0]->getCommission();
  $finalAmountFinal += $finalAmount;
  ?><tr>
    <td class="td"><?php echo $commissionDetails[0]->getProjectName()  ?></td>
  <td class="td"><?php echo $commissionDetails[0]->getDetails()  ?></td>
      <td class="td"><?php echo $commissionDetails[0]->getUnitNo()  ?></td>
    <td class="td"><?php echo number_format($loanDetails[0]->getNettPrice(),2)  ?></td>
    <td class="td"><?php echo number_format($commissionDetails[0]->getCommission(), 2)  ?></td>
</tr>
<?php  }
 for ($i=count($idExplode); $i < 5 ; $i++) {
  ?><tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr> <?php
}
?><td class="td"></td>
<td class="td"></td>
<td class="td"></td>
<td class="td"><b>Total :</b></td>
<td class="td"><b><?php echo number_format($finalAmountFinal, 2)  ?></b></td>
</tr><?php
}elseif (isset($_POST['claim_id']) && isset($_POST['claim_type'])) {
  $claimType = $_POST['claim_id'];
  $commDetails = getCommission($conn, "WHERE claim_id = ?",array("claim_id"),array($claimType), "s");
  for ($cnt=0; $cnt <count($commDetails) ; $cnt++) {
  // $commissionDetails = getCommission($conn, "WHERE id = ?",array("id"),array($idExplode[$cnt]),"s");
  // $loanUid = $commissionDetails[0]->getLoanUid();
  // $loanDetails = getLoanStatus($conn, "WHERE loan_uid = ?",array("loan_uid"),array($loanUid), "s");
  // $finalAmount = $commissionDetails[0]->getCommission();
  // $finalAmountFinal += $finalAmount;
  ?><tr>
    <td class="td"><?php echo $commDetails[$cnt]->getProjectName()  ?></td>
  <td class="td"><?php echo $commDetails[$cnt]->getDetails()  ?></td>
      <td class="td"><?php echo $commDetails[$cnt]->getUnitNo()  ?></td>
    <td class="td"><?php echo number_format($loanDetails[0]->getNettPrice(),2)  ?></td>
    <td class="td"><?php echo number_format($commDetails[$cnt]->getCommission(), 2)  ?></td>
</tr>
<?php  }
}
else {
                ?><tr>
                  <td class="td"><?php echo $commissionDetails[0]->getProjectName()  ?></td>
                <td class="td"><?php echo $commissionDetails[0]->getDetails()  ?></td>
                    <td class="td"><?php echo $commissionDetails[0]->getUnitNo()  ?></td>
                  <td class="td"><?php echo number_format($nettPrice,2)  ?></td>
                  <td class="td"><?php echo number_format($commissionDetails[0]->getCommission(), 2)  ?></td>
              </tr>
              <tr>
                <td class="td"></td>
                  <td class="td"></td>
                <td class="td"></td>
                <td class="td"></td>
                <td class="td"></td>
              </tr>
              <tr>
              <td class="td"></td>
                <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              </tr>
              <tr>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              </tr>
              <tr>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"><b>Total :</b></td>
              <td class="td"><b><?php echo number_format($commissionDetails[0]->getCommission(), 2)  ?></b></td>
              </tr><?php
} ?>


        </table>
		<div class="clear"></div>


        <div class="width100">
			<table  class="vtop-data" >
            	<tr>
                	<td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                	<td>Total in Ringggit Malaysia</td>
                    <td><b>:</b></td>
                    <td><?php   if (isset($_POST['headerCheck']) && isset($_POST['selectedCommSlip']) || isset($_POST['claim_id']) && isset($_POST['claim_type'])) {
                      $claimType = $_POST['claim_id'];
                      $commDetails = getCommission($conn, "WHERE claim_id = ?",array("claim_id"),array($claimType), "s");
                      for ($cnt=0; $cnt <count($commDetails) ; $cnt++) {
                        $a = $commDetails[$cnt]->getCommission();
                        $af += $a;

                    }
                        $num = str_replace(",","",number_format($af,2));
                        $split = explode('.',$num);
                        $whole = convertNumber($split[0].".0");
                        $cents = convertNumber($split[1].".0");
                        echo mb_strtoupper($whole." Ringgit and ".$cents." cents");
                      }else {
                        $num = str_replace(",","",number_format($commissionDetails[0]->getCommission(),2));
                        $split = explode('.',$num);
                        $whole = convertNumber($split[0].".0");
                        $cents = convertNumber($split[1].".0");
                        echo mb_strtoupper($whole." Ringgit and ".$cents." cents");
                      } ?></td>
                </tr>
                <tr>
                	<td>Online Transfer</td>
                    <td><b>:</b></td>
                    <td>4645646</td>
                </tr>

            </table>
        </div>
        <div class="clear"></div>
        <div class="invoice-print-spacing"></div>
        <div style="margin-bottom: 0 auto" class="signature-div left-sign">
        	<div class="signature-border"></div>
            <p class="invoice-p">Approved by:</p>
            <p class="invoice-p"><b>Eddie Song</b></p>
        </div>
        <div style="margin-bottom: 0 auto" class="signature-div right-sign">
        	<div class="signature-border"></div>
            <p class="invoice-p">Received by:</p>
            <p class="invoice-p"><b>Name</b></p>
        </div>

	<div class="clear"></div>
    <?php if ($_SESSION['usertype_level'] == 3) {
      ?><div class="dual-button-div width100">
      	<a href="#">
              <button style="margin-left: 360px" class="mid-button red-btn clean"  onclick="window.print()">
                  Print
              </button>
          </a>
      </div><?php
    }elseif(isset($_POST['submit'])) {
      ?><div class="dual-button-div width100">
      	<a href="#">
              <div class="left-button1  white-red-line-btn">
                  Edit
              </div>
          </a>
      	<a href="#">
              <button class="mid-button red-btn clean"  onclick="window.print()">
                  Print
              </button>
          </a>
          <form class="" action="utilities/sendToAgent.php" method="post">
            <?php if(isset($_POST['selectedCommSlip']) && isset($_POST['headerCheck'])){
              ?><input type="hidden" name="id" value="<?php echo $idImplode ?>">
              <input type="hidden" name="claim_type" value="<?php echo $claimType ?>">
              <input type="hidden" name="claim_id" value="<?php echo $claimId ?>"><?php
            }else {
              ?><input type="hidden" name="id" value="<?php echo $id ?>">
              <input type="hidden" name="claim_type" value="<?php echo $claimType ?>">
              <input type="hidden" name="claim_id" value="<?php echo $claimId ?>"><?php
            } ?>
            <button type="submit" name="sendToAgent" class="clean white-red-line-btn right-button2" >
                Send Commission
            </button>
          </form>
      </div><?php
    } ?>


</div></div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
}else {
  $_SESSION['messageType'] = 5;
  header('location: ./agentDashboard.php?type=5');
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function goBack() {
  window.history.back();
}
</script>
</body>
</html>
