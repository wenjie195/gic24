<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/adminAccess1.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/PaymentMethod.php';
require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Test jQuery</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <?php include 'css.php'; ?>
    <style media="screen">
      #addBtn{
        width: 10px;
      }
    </style>
  </head>
  <body>
    <form class="" action="next.php" method="post">


    <img id="addBtn" src="img/add-referee.png">

    <div id="addIn" class="dual-input-div">
        <p>Purchaser Name</p>
        <input class="dual-input clean" type="text" placeholder="Purchaser Name" id="purchaser_name" name="purchaser_name[]" required>
      </div>
      <div class="test">

      </div>

      <button type="submit" name="upload">Submit</button>
  </form>



  </body>
  <script type="text/javascript">
    $(document).ready( function(){
      $("#addBtn").click( function(){
        var html = '<div id="addIn" class="dual-input-div"><p>Purchaser Name</p><input class="dual-input clean" type="text" placeholder="Purchaser Name" id="purchaser_name" name="purchaser_name[]" required></div>';
        $(html).hide().appendTo(".test").fadeIn();
      });
    });
  </script>
  <!-- <script>
  $(document).ready(function(){
    $("#addBtn").click(function(){
      $("#addIn").append('<div><p>Purchaser Name</p><input class="dual-input clean" type="text" placeholder="Purchaser Name" name="purchaser_name[]" required></div>');
    });
  });
  </script> -->
</html>
