<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$username = $_SESSION['username'];

$conn = connDB();

$userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
$userDetails = $userRows[0];
$userUsername = $userDetails->getUsername();

// echo $userUsername ;

// $projectDetails = getProject($conn);
// $projectName = "";
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">

<h1 class="h1-title h1-before-border shipping-h1">My Performance</h1>
  <!-- <h1 class="h1-title h1-before-border shipping-h1">Personal Sales (SPA Signed)</h1> -->


  <div class="short-red-border"></div>

<h3 class="h1-title"><a href="agentDashboard.php"> Personal Sales</a> | Personal Overriding</h3>

  <div class="clear"></div>
  <div class="section-divider width100 overflow">

  <?php
  $conn = connDB();

//   $projectName = "";
  $projectName = " ";

  $projectDetails = getProject($conn);
  ?>

  <form class="" action="selected.php" method="post">
      <select id="sel_id" name="personalOverriding"  onchange="this.form.submit();" class="clean-select">
          <?php if (isset($_GET['name']))
          {
              if ($_GET['name'] == 'SHOW ALL')
              {
                // $projectName = "";
                // $projectName = "WHERE case_status = 'COMPLETED' ";
                  $projectName = "";
                // $projectName = "WHERE agent = '$userUsername' and dateCreated = DESC";
              }
              else
              {
                  $type = $_GET['name'];
                  $types = urldecode("$type");
                  // $projectName = "WHERE project_name = '$types' and case_status = 'COMPLETED' ";
                  $projectName = "project_name = '$types' AND";
              }
              ?>
              <option value="">
                  <?php echo $_GET['name'] ?>
              </option>
              <option value="">--</option>
              <?php
          }
          else
          {
              ?>
              <option value="">   Choose Project  </option>
              <?php
          }
          ?>

          <?php if ($projectDetails)
          {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++)
          {
              ?>
                  <option value="<?php echo $projectDetails[$cnt]->getProjectName()?>">
                      <?php echo $projectDetails[$cnt]->getProjectName() ?>
                  </option>
              <?php
          }
          ?>
              <option value="SHOW ALL">   SHOW ALL    </option>
          <?php
          }
          $conn->close();
          ?>
      </select>
  </form>
  </div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th class="th">NO.</th>
                        <th class="th">PROJECT NAME</th>
                        <th class="th">UNIT NO.</th>
                        <th class="th">BOOKING DATE</th>
                        <th class="th">SPA PRICE (RM)</th>
                        <th class="th">NETT PRICE (RM)</th>
                        <th class="th">OVERRIDING COMMISSION</th>

                        <!-- <th class="th"><?php //echo wordwrap("NO.",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("PROJECT NAME",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("UNIT NO.",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("BOOKING DATE",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("NETT PRICE",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("AGENT",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("CASE STATUS",10,"</br>\n");?></th> -->

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = connDB();
                        $upline1Details = getLoanStatus($conn," WHERE $projectName upline1 = ?",array("upline1"),array($userUsername), "s");
                        $upline2Details = getLoanStatus($conn," WHERE $projectName upline2 = ?",array("upline2"),array($userUsername), "s");
                        $plNameDetails = getLoanStatus($conn," WHERE $projectName pl_name = ?",array("pl_name"),array($userUsername), "s");
                        $hosNameDetails = getLoanStatus($conn," WHERE $projectName hos_name = ?",array("hos_name"),array($userUsername), "s");
                        $listerNameDetails = getLoanStatus($conn," WHERE $projectName lister_name = ?",array("lister_name"),array($userUsername), "s");
                        $no = 1;
                        // $upline1Details = getLoanStatus($conn, $projectName);
                        if($upline1Details != null)
                        {
                            for($cntAA = 0;$cntAA < count($upline1Details) ;$cntAA++)
                            {?>
                            <tr>
                                <td class="td"><?php echo ($no)?></td>
                                <td class="td"><?php echo $upline1Details[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $upline1Details[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo date('d-m-Y', strtotime($upline1Details[$cntAA]->getBookingDate()));?></td>
                                <td class="td"><?php echo $upline1Details[$cntAA]->getSpaPrice();?></td>
                                <td class="td"><?php echo $upline1Details[$cntAA]->getNettPrice();?></td>
                                <?php if ( $upline1Details[$cntAA]->getUlOverride()) {
                                  ?><td class="td"><?php echo $upline1Details[$cntAA]->getUlOverride();?></td><?php
                                } ?>

                            </tr>
                            <?php
                            $no += 1;
                            }
                        }
                        if($upline2Details != null)
                        {
                            for($cntAA = 0;$cntAA < count($upline2Details) ;$cntAA++)
                            {?>
                            <tr>
                                <td class="td"><?php echo ($no)?></td>
                                <td class="td"><?php echo $upline2Details[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $upline2Details[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo date('d-m-Y', strtotime($upline2Details[$cntAA]->getBookingDate()));?></td>
                                <td class="td"><?php echo $upline2Details[$cntAA]->getSpaPrice();?></td>
                                <td class="td"><?php echo $upline2Details[$cntAA]->getNettPrice();?></td>
                                <?php if ( $upline2Details[$cntAA]->getUulOverride()) {
                                  ?><td class="td"><?php echo $upline2Details[$cntAA]->getUulOverride();?></td><?php
                                } ?>

                            </tr>
                            <?php
                            $no += 1;
                            }
                        }
                        if($plNameDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($plNameDetails) ;$cntAA++)
                            {?>
                            <tr>
                                <td class="td"><?php echo ($no)?></td>
                                <td class="td"><?php echo $plNameDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $plNameDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo date('d-m-Y', strtotime($plNameDetails[$cntAA]->getBookingDate()));?></td>
                                <td class="td"><?php echo $plNameDetails[$cntAA]->getSpaPrice();?></td>
                                <td class="td"><?php echo $plNameDetails[$cntAA]->getNettPrice();?></td>
                                <?php if ( $plNameDetails[$cntAA]->getUlOverride()) {
                                  ?><td class="td"><?php echo $plNameDetails[$cntAA]->getPlOverride();?></td><?php
                                } ?>

                            </tr>
                            <?php
                            $no += 1;
                            }
                        }
                        if($listerNameDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($listerNameDetails) ;$cntAA++)
                            {?>
                            <tr>
                                <td class="td"><?php echo ($no)?></td>
                                <td class="td"><?php echo $listerNameDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $listerNameDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo date('d-m-Y', strtotime($listerNameDetails[$cntAA]->getBookingDate()));?></td>
                                <td class="td"><?php echo $listerNameDetails[$cntAA]->getSpaPrice();?></td>
                                <td class="td"><?php echo $listerNameDetails[$cntAA]->getNettPrice();?></td>
                                <?php if ( $listerNameDetails[$cntAA]->getUlOverride()) {
                                  ?><td class="td"><?php echo $listerNameDetails[$cntAA]->getListerOverride();?></td><?php
                                } ?>

                            </tr>
                            <?php
                            $no += 1;
                            }
                        }
                        if($hosNameDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($hosNameDetails) ;$cntAA++)
                            {?>
                            <tr>
                                <td class="td"><?php echo ($no)?></td>
                                <td class="td"><?php echo $hosNameDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $hosNameDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo date('d-m-Y', strtotime($hosNameDetails[$cntAA]->getBookingDate()));?></td>
                                <td class="td"><?php echo $hosNameDetails[$cntAA]->getSpaPrice();?></td>
                                <td class="td"><?php echo $hosNameDetails[$cntAA]->getNettPrice();?></td>
                                <?php if ( $hosNameDetails[$cntAA]->getUlOverride()) {
                                  ?><td class="td"><?php echo $hosNameDetails[$cntAA]->getHosOverride();?></td><?php
                                } ?>

                            </tr>
                            <?php
                            $no += 1;
                            }
                        }

                    ?>
                </tbody>
            </table><br>
    </div>

    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
